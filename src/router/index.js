import Vue from 'vue'
import Router from 'vue-router'
import Add from "@/components/Add.vue";
import Delete from "@/components/Delete.vue";
import Raffle from "@/components/Raffle.vue";
import View from "@/components/View.vue";
import Login from "@/components/Login.vue";
import Register from "@/components/Register.vue";

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/add',
      name: 'Add',
      component:Add
    },
    {
      path:'/delete/:id',
      name:'Delete',
      component:Delete
    },
    {
      path:'/raffle',
      name:'Raffle',
      component:Raffle
    },
    {
      path:'/view',
      name:'View',
      component:View
    },
    {
      path:'/',
      name:'Login',
      component:Login
    },
    {
      path:'/reg',
      name:'Register',
      component:Register
    }
  ],



  mode:'history'
})
